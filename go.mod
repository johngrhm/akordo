module gitlab.com/technonauts/akordo

go 1.14

require (
	github.com/bwmarrin/discordgo v0.20.3
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79 // indirect
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
